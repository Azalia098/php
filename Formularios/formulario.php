<?php session_start();
if(empty($_SESSION['num_cta'])){ 		
	   header('location: index.php');
   } elseif($_SESSION['num_cta']!='1'){
		echo "No tiene permiso de agregar usuarios<br>";
		echo "<a href='./info.php'>Mostrar todo</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>";
    	echo "<a href='formulario.php'>Agregar un alumno</a><br>";
   		echo "<a href='cerrar_sesion.php'>Cerrar sesion</a><br>";
		die();
   }
?>

<html>
<head>
<title>Formulario</title>
<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/styles.css">
<meta charset="UTF-8">
</head>
<body>
<nav>
		<a href='./index.php'>Iniciar Sesión</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='./formulario.php'>Agregar un alumno</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='./info.php'>Mostrar usuarios</a>
		<a href='./cerrar_sesion.php'>Cerrar sesion</a>
	</nav>
    <div class="container">
        <div class="columns">
            <form action="captura_formulario.php?accion=get&texto=textoenget" method="POST" >
				<div class="contenedor-formulario2 pintar">
                <label class="form-label" for="input-text">Número de cuenta</label>
				<input name="num_cta" class="form-input " type="number" min="1" id="input-num_cta" placeholder="Numero_de_cuenta"><br>
                <label class="form-label" for="input-text">Nombre</label>
				<input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="Nombre"><br>
                <label class="form-label" for="input-text">Primer apellido</label>
				<input name="primer_apellido" class="form-input " type="text" id="input-apell1" placeholder="primer_apellido"><br>
                <label class="form-label" for="input-text">Segundo apellido</label>
				<input name="segundo_apellido" class="form-input " type="text" id="input-apell2" placeholder="segundo_apellido"><br>				
				<label class="form-label">Genero</label><br>
				<label class="form-radio">
					<input type="radio" name="genero" value="H" checked>
					<i class="form-icon"></i> Hombre
				</label><br>
				<label class="form-radio">
					<input type="radio" name="genero" value="F">
					<i class="form-icon"></i> Mujer
				</label><br>         
                <label class="form-radio">
					<input type="radio" name="genero" value="O" checked>
					<i class="form-icon"></i> Otro
				</label><br>       
                <label class="form-label" for="input-date">Fecha</label>
				<input name="date" class="form-input " type="date" id="input-date" placeholder="fecha"><br>
                <label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña"><br>
   				<div class="flex">
				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn" value="Limpiar"/> 
				</div>      
				</div>    
            </form>
        </div>
    </div>
</body>
</html>
