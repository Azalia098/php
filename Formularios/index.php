<html> 
<head>
<title>Login</title>
        <link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/styles.css">
<meta charset="UTF-8">
</head>
<body>
<nav>
		<a href='./index.php'>Iniciar Sesión</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='./formulario.php'>Agregar un alumno</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='./info.php'>Mostrar usuarios</a>
		<a href='./cerrar_sesion.php'>Cerrar sesion</a>
    </nav>

    <form action="login.php?accion=get&texto=textoenget" method="POST" >
        <div class="contenedor-formulario pintar">
        <h1>Login</h1>
        <label class="form-label" for="input-text">Número de cuenta</label>
		<input name="num_cta" class="form-input " type="number" min="1" id="input-num_cta" placeholder="Numero_de_cuenta"><br>
        <label class="form-label" for="input-password">Contraseña</label>
		<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña"><br>
        <div class="flex">
        <input type='submit' class="btn" value="Enviar"/>
		<input type='reset' class="btn btn-primary" value="Limpiar"/>  
        </div>
    </div>
</body>
</html>

<?php session_start();

if(empty($_SESSION)){
    header('location: sesiones.php');
}

?>