 <?php
 function escape_caracter($exp,$caracter){
    
    if($caracter=='$'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\$]{1,})$/',$exp);
     } else if($caracter=='+'){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\+]{1,})$/',$exp);
}    else if($caracter=='*'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\*]{1,})$/',$exp);
}
else if($caracter=='?'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\?]{1,})$/',$exp);
} else if($caracter=='['){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\[]{1,})$/',$exp);
} else if($caracter=='^'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\^]{1,})$/',$exp);
}
else if($caracter==']'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\]]{1,})$/',$exp);
    return $result;
}else if($caracter=='('){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\(]{1,})$/',$exp);
 } else if($caracter==')'){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\)]{1,})$/',$exp);
    } else if($caracter=='{'){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\{]{1,})$/',$exp);
    } else if($caracter=='}'){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\}]{1,})$/',$exp);
    } else if($caracter=='='){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\=)]{1,})$/',$exp);
    } else if($caracter=='!'){
    $exp;
    $result=preg_match('/^([a-zA-Z0-9\!)]{1,})$/',$exp);
    }    else if($caracter=='<'){
        $exp;
        $result=preg_match('/^([a-zA-Z0-9\<)]{1,})$/',$exp);
        }else if($caracter=='>'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\>)]{1,})$/',$exp);
        } else if($caracter=='<'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\<)]{1,})$/',$exp);
        } else if($caracter=='|'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\|)]{1,})$/',$exp);
        } else if($caracter==':'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\:)]{1,})$/',$exp);
        }else if($caracter=='-'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\-)]{1,})$/',$exp);
        } else if($caracter=='.'){
            $exp;
            $result=preg_match('/^([a-zA-Z0-9\.]{1,})$/',$exp);
}
return $result;
}
//Numero 1. Email correcto

function email($check){
$result=preg_match('/^[a-z0-9\._-]{1,}+@([a-z]{2,6})\.([a-z]{2,6})((\.([a-z]{2,5}))*)$/', $check);
if($result){
echo "Correo: ".$check." es: correcto<br>";} else{
    echo "Correo: ".$check." es: incorrecto<br>";
}}
$check="azalia.noir@gmail.com.mx";
email($check);
$check="azalia.noir@gmail.com";
email($check);
$check="azalia.noir@.com.mx";
email($check);
//Numero 2. CURP: ABCD123456EFGHIJ78

function curp($check){
$result=preg_match('/^(([A-Z]){4}[\d]{6}([A-Z]){6}[\d]{2})$/i',$check);
if($result){
echo "CURP: ".$check." es: correcto<br>";
}else{
    echo "CURP: ".$check." es: incorrecto<br>";
}}
$check='ABCD123456EFGHIJ78';
curp($check);
$check='ABC99123456RFGHIJ78';
curp($check);
//Numero 3.  Palabras de longitud mayor a 50 formadas solo por letras 

function longitud($check){
$result=preg_match('/^(([a-zA-Z]){50,})$/i',$check);
if($result){
echo "Palabra: ".$check." es mayor que 50: correcto<br>";
} else{
    echo "Palabra: ".$check." es mayor que 50: incorrecto<br>";
}}
$check="aaabbbbbbbbbbbbbbbbbbbbbbbbbZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZddddddddddddddddddddd";
longitud($check);
$check="aa";
longitud($check);
$check="aaabb22bbbbbbbbbbbbbbbbbbbbbZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZddddddddddddddddddddd";
longitud($check);
//Numero 4. Crea una función para escapar los símbolos especiales.
    function escape($exp,$caracter){
        $result=escape_caracter($exp,$caracter);
    if($result){
print("Escape de la cadena: ".$exp." del simbolo ".$caracter." es correcto<br>");
    }else{
        print("Escape de la cadena: ".$exp." del simbolo ".$caracter." es incorrecto <br>");
    }}
    $exp="aaaa?0000?aaa";
    $caracter="?";
    escape($exp,$caracter);
    $exp="aaaa00||00aaa";
    $caracter="?";
    escape($exp,$caracter);
    $exp="aaaa|0000|aaa";
    $caracter="|";
    escape($exp,$caracter);
    $exp="aaaa0000aaa";
    $caracter="|";
    escape($exp,$caracter);
    $result=escape_caracter($exp,$caracter);

//Numero 5.Crear una expresión regular para detectar números decimales.

function decimal($check){
$result=preg_match('/^(([\d]){1,})\.(([\d]){1,})$/',$check);
if($result){
echo "Número decimal: ".$check." es: correcto<br>";
}else{
    echo "Número decimal: ".$check." es: incorrecto<br>";
}
}
$check="6288.7699";
decimal($check);
$check="6288";
decimal($check);
$check="6288.7699.999";
decimal($check);

?>